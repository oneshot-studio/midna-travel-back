import { Router } from 'express';
import FlightsControllet from '@controllers/flights.controller';
import { CreateUserDto } from '@dtos/users.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';

class FlightRoute implements Routes {
  public path = '/flights';
  public router = Router();
  public flightController = new FlightsControllet();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.flightController.getFlights);
    this.router.get(`${this.path}/history`, this.flightController.getFlights);
    // this.router.get(`${this.path}/:id(\\d+)`, this.flightController.getUserById);
    // this.router.post(`${this.path}`, validationMiddleware(CreateUserDto, 'body'), this.flightController.createUser);
  }
}

export default FlightRoute;

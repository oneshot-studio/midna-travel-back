import { Router } from 'express';
import FeedController from '@/controllers/feed.controller';
import { CreateAirlineDto } from '@/dtos/airline.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';

class FeedRoute implements Routes {
  public path = '/feed';
  public router = Router();
  public feedController = new FeedController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.feedController.getAllFeed);
    this.router.get(`${this.path}/mongo`, this.feedController.testMongo);
    // this.router.get(`${this.path}/history`, this.flightController.getFlights);
    // this.router.get(`${this.path}/:id(\\d+)`, this.flightController.getUserById);
    // this.router.post(`${this.path}`, validationMiddleware(CreateUserDto, 'body'), this.flightController.createUser);
  }
}

export default FeedRoute;

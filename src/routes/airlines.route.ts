import { Router } from 'express';
import AirlineController from '@/controllers/airlines.controller';
import { CreateAirlineDto } from '@/dtos/airline.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';

class AirlinesRoute implements Routes {
  public path = '/airlines';
  public router = Router();
  public airlineController = new AirlineController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.airlineController.getAirlines);
    // this.router.get(`${this.path}/history`, this.flightController.getFlights);
    // this.router.get(`${this.path}/:id(\\d+)`, this.flightController.getUserById);
    // this.router.post(`${this.path}`, validationMiddleware(CreateUserDto, 'body'), this.flightController.createUser);
  }
}

export default AirlinesRoute;

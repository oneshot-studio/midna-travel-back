import { Feed } from '@/interfaces/feed.interface';

// password: password
export const airlinesModel: Feed[] = [
  {
    content:
      'Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum ',
    img: 'https://foodandtravel.mx/wp-content/uploads/2019/09/AsiesSonoraPic.jpg',
    title: 'Viaja a lorem ipsum',
  },
  {
    content:
      'Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum ',
    img: 'https://www.nationalgeographic.com.es/medio/2018/02/27/playa-de-isuntza-lekeitio__1280x720.jpg',
    title: 'Viaja a lorem ipsum',
  },
  {
    content:
      'Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum ',
    img: 'https://ladefinicion.com/wp-content/uploads/2019/08/paisaje-natural.jpg',
    title: 'Viaja a lorem ipsum',
  },
  {
    content:
      'Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum ',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTbYIAgg2vguCryROahby2uyoRcB3cu1cqmeg&usqp=CAU',
    title: 'Viaja a lorem ipsum',
  },
  {
    content:
      'Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum ',
    img: 'https://pbs.twimg.com/media/EurxiUlWYAYcMIT.jpg',
    title: 'Viaja a lorem ipsum',
  },
  {
    content:
      'Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum ',
    img: 'https://www.viajedechina.com/pic/guia-de-china/mejores-de-china/paisajes-naturales-mas-hermosos-de-china-02.jpg',
    title: 'Viaja a lorem ipsum',
  },
  {
    content:
      'Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum ',
    img: 'https://www.blogdelfotografo.com/wp-content/uploads/2014/08/61.jpg',
    title: 'Viaja a lorem ipsum',
  },
  {
    content:
      'Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum ',
    img: 'https://i0.wp.com/blog.vivaaerobus.com/wp-content/uploads/2020/04/paisaje-de-los-cabos.jpg?resize=650%2C364&ssl=1',
    title: 'Viaja a lorem ipsum',
  },
  {
    content:
      'Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum \n Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum Viaja a lorem ipsum ',
    img: 'https://3.bp.blogspot.com/-ZvlKRHyhank/UQgIukZUHeI/AAAAAAAAW_k/TV7O-w4dX_U/s1600/lindos+paisajes+naturales++.jpg',
    title: 'Viaja a lorem ipsum',
  },
];

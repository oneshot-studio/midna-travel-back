import { Flight } from '@/interfaces/flight.interface';

// password: password
export const flightModel: Flight = {
  type: 'FeatureCollection',
  features: [
    {
      id: '8031b7dc-21c9-4026-a295-0a09c6930828',
      type: 'Feature',
      geometry: null,
      properties: {
        airline: 'VIV',
        arrival: {
          aerodrome: {
            initial: 'MMMY',
            scheduled: 'MMMY',
          },
          runwayTime: {
            initial: '2022-05-20T16:45:00Z',
            estimated: '2022-05-20T16:45:00Z',
          },
        },
        callsign: 'VIV4111',
        departure: {
          aerodrome: {
            actual: 'MMHO',
            scheduled: 'MMHO',
          },
          runwayTime: {
            initial: '2022-05-20T14:45:00Z',
          },
        },
        flightStatus: 'PLANNED',
        iataFlightNumber: 'VB4111',
        timestampProcessed: '2022-05-19T14:46:37Z',
        aircraftDescription: {
          aircraftCode: 'A321',
        },
      },
    },
    {
      id: 'f879f8b6-c47e-44da-85b3-90b3ed69f0cc',
      type: 'Feature',
      geometry: null,
      properties: {
        airline: 'VIV',
        arrival: {
          aerodrome: {
            initial: 'MMMY',
            scheduled: 'MMMY',
          },
          runwayTime: {
            initial: '2022-05-22T16:45:00Z',
          },
        },
        departure: {
          aerodrome: {
            actual: 'MMHO',
            scheduled: 'MMHO',
          },
          runwayTime: {
            initial: '2022-05-22T14:45:00Z',
          },
        },
        flightStatus: 'SCHEDULED',
        iataFlightNumber: 'VB4111',
        timestampProcessed: '2022-05-19T22:19:02Z',
        aircraftDescription: {
          aircraftCode: 'A321',
        },
      },
    },
    {
      id: 'f879f8b6-c47e-44da-85b3-90b3ed69f0cc',
      type: 'Feature',
      geometry: null,
      properties: {
        airline: 'VIV',
        arrival: {
          aerodrome: {
            initial: 'MMMY',
            scheduled: 'MMMY',
          },
          runwayTime: {
            initial: '2022-05-22T16:45:00Z',
          },
        },
        departure: {
          aerodrome: {
            actual: 'MMHO',
            scheduled: 'MMHO',
          },
          runwayTime: {
            initial: '2022-05-22T14:45:00Z',
          },
        },
        flightStatus: 'SCHEDULED',
        iataFlightNumber: 'VB4111',
        timestampProcessed: '2022-05-19T22:19:02Z',
        aircraftDescription: {
          aircraftCode: 'A321',
        },
      },
    },
    {
      id: 'f879f8b6-c47e-44da-85b3-90b3ed69f0cc',
      type: 'Feature',
      geometry: null,
      properties: {
        airline: 'VIV',
        arrival: {
          aerodrome: {
            initial: 'MMMY',
            scheduled: 'MMMY',
          },
          runwayTime: {
            initial: '2022-05-22T16:45:00Z',
          },
        },
        departure: {
          aerodrome: {
            actual: 'MMHO',
            scheduled: 'MMHO',
          },
          runwayTime: {
            initial: '2022-05-22T14:45:00Z',
          },
        },
        flightStatus: 'SCHEDULED',
        iataFlightNumber: 'VB4111',
        timestampProcessed: '2022-05-19T22:19:02Z',
        aircraftDescription: {
          aircraftCode: 'A321',
        },
      },
    },
  ],
  results: {
    publishTime: '2022-05-20T00:13:58.191179Z',
    status: 'ok',
    total: 2,
  },
};

export const flightModelHistory: Flight = {
  type: 'FeatureCollection',
  features: [
    {
      id: '8031b7dc-21c9-4026-a295-0a09c6930828',
      type: 'Feature',
      geometry: null,
      properties: {
        airline: 'VIV',
        arrival: {
          aerodrome: {
            initial: 'MMMY',
            scheduled: 'MMMY',
          },
          runwayTime: {
            initial: '2022-05-20T16:45:00Z',
            estimated: '2022-05-20T16:45:00Z',
          },
        },
        callsign: 'VIV4111',
        departure: {
          aerodrome: {
            actual: 'MMHO',
            scheduled: 'MMHO',
          },
          runwayTime: {
            initial: '2022-05-20T14:45:00Z',
          },
        },
        flightStatus: 'PLANNED',
        iataFlightNumber: 'VB4111',
        timestampProcessed: '2022-05-19T14:46:37Z',
        aircraftDescription: {
          aircraftCode: 'A321',
        },
      },
    },
    {
      id: 'f879f8b6-c47e-44da-85b3-90b3ed69f0cc',
      type: 'Feature',
      geometry: null,
      properties: {
        airline: 'VIV',
        arrival: {
          aerodrome: {
            initial: 'MMMY',
            scheduled: 'MMMY',
          },
          runwayTime: {
            initial: '2022-05-22T16:45:00Z',
          },
        },
        departure: {
          aerodrome: {
            actual: 'MMHO',
            scheduled: 'MMHO',
          },
          runwayTime: {
            initial: '2022-05-22T14:45:00Z',
          },
        },
        flightStatus: 'SCHEDULED',
        iataFlightNumber: 'VB4111',
        timestampProcessed: '2022-05-19T22:19:02Z',
        aircraftDescription: {
          aircraftCode: 'A321',
        },
      },
    },
    {
      id: 'f879f8b6-c47e-44da-85b3-90b3ed69f0cc',
      type: 'Feature',
      geometry: null,
      properties: {
        airline: 'VIV',
        arrival: {
          aerodrome: {
            initial: 'MMMY',
            scheduled: 'MMMY',
          },
          runwayTime: {
            initial: '2022-05-22T16:45:00Z',
          },
        },
        departure: {
          aerodrome: {
            actual: 'MMHO',
            scheduled: 'MMHO',
          },
          runwayTime: {
            initial: '2022-05-22T14:45:00Z',
          },
        },
        flightStatus: 'SCHEDULED',
        iataFlightNumber: 'VB4111',
        timestampProcessed: '2022-05-19T22:19:02Z',
        aircraftDescription: {
          aircraftCode: 'A321',
        },
      },
    },
    {
      id: 'f879f8b6-c47e-44da-85b3-90b3ed69f0cc',
      type: 'Feature',
      geometry: null,
      properties: {
        airline: 'VIV',
        arrival: {
          aerodrome: {
            initial: 'MMMY',
            scheduled: 'MMMY',
          },
          runwayTime: {
            initial: '2022-05-22T16:45:00Z',
          },
        },
        departure: {
          aerodrome: {
            actual: 'MMHO',
            scheduled: 'MMHO',
          },
          runwayTime: {
            initial: '2022-05-22T14:45:00Z',
          },
        },
        flightStatus: 'SCHEDULED',
        iataFlightNumber: 'VB4111',
        timestampProcessed: '2022-05-19T22:19:02Z',
        aircraftDescription: {
          aircraftCode: 'A321',
        },
      },
    },
    {
      id: 'f879f8b6-c47e-44da-85b3-90b3ed69f0cc',
      type: 'Feature',
      geometry: null,
      properties: {
        airline: 'VIV',
        arrival: {
          aerodrome: {
            initial: 'MMMY',
            scheduled: 'MMMY',
          },
          runwayTime: {
            initial: '2022-05-22T16:45:00Z',
          },
        },
        departure: {
          aerodrome: {
            actual: 'MMHO',
            scheduled: 'MMHO',
          },
          runwayTime: {
            initial: '2022-05-22T14:45:00Z',
          },
        },
        flightStatus: 'SCHEDULED',
        iataFlightNumber: 'VB4111',
        timestampProcessed: '2022-05-19T22:19:02Z',
        aircraftDescription: {
          aircraftCode: 'A321',
        },
      },
    },
    {
      id: 'f879f8b6-c47e-44da-85b3-90b3ed69f0cc',
      type: 'Feature',
      geometry: null,
      properties: {
        airline: 'VIV',
        arrival: {
          aerodrome: {
            initial: 'MMMY',
            scheduled: 'MMMY',
          },
          runwayTime: {
            initial: '2022-05-22T16:45:00Z',
          },
        },
        departure: {
          aerodrome: {
            actual: 'MMHO',
            scheduled: 'MMHO',
          },
          runwayTime: {
            initial: '2022-05-22T14:45:00Z',
          },
        },
        flightStatus: 'SCHEDULED',
        iataFlightNumber: 'VB4111',
        timestampProcessed: '2022-05-19T22:19:02Z',
        aircraftDescription: {
          aircraftCode: 'A321',
        },
      },
    },
  ],
  results: {
    publishTime: '2022-05-20T00:13:58.191179Z',
    status: 'ok',
    total: 2,
  },
};

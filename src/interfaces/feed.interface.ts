export interface Feed {
  title: string;
  content: string;
  img: string;
}

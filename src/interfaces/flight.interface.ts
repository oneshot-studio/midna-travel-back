export interface FlightFeature {
  id: string;
  type: string;
  geometry: unknown;
  properties: {
    airline: string;
    arrival: {
      aerodrome: {
        initial: string;
        scheduled: string;
      };
      runwayTime: {
        initial: string;
        estimated: string;
      };
    };
    callsign: string;
    departure: {
      aerodrome: {
        actual: string;
        scheduled: string;
      };
      runwayTime: {
        initial: string;
      };
    };
    flightStatus: string;
    iataFlightNumber: string;
    timestampProcessed: string;
    aircraftDescription: {
      aircraftCode: string;
    };
  };
}

export interface Flight {
  type: string;
  features: FlightFeature[];
  results: {
    publishTime: string;
    status: string;
    total: number;
  };
}

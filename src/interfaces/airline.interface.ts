export interface Feed {
  IATA: string;
  ICAO: string;
  Airline: string;
  CallSign: string;
  CountryRegion: string;
  Comments: string;
}
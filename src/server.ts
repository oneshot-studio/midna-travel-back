import App from '@/index';
import AuthRoute from '@routes/auth.route';
import IndexRoute from '@routes/index.route';
import UsersRoute from '@routes/users.route';
import FlightRoute from '@/routes/flights.route';
import AirlinesRoute from './routes/airlines.route';
import FeedRoute from './routes/feed.route';
import validateEnv from '@utils/validateEnv';

validateEnv();

const app = new App([new IndexRoute(), new UsersRoute(), new AuthRoute(), new FlightRoute(), new AirlinesRoute(), new FeedRoute()]);

app.listen();

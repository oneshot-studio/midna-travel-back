import { NextFunction, Request, Response } from 'express';
import { CreateFlightDto } from '@dtos/flight.dto';
import { Flight } from '@interfaces/flight.interface';
import FlightService from '@services/flight.service';

class FlightsController {
  public flightService = new FlightService();

  public getFlights = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const data: Flight = await this.flightService.findAllflights();
      setTimeout(() => {
        res.status(200).json(data);
      }, 1000);
    } catch (error) {
      next(error);
    }
  };

  public getFlightsHostory = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const data: Flight = await this.flightService.findAllflightsHistory();
      setTimeout(() => {
        res.status(200).json(data);
      }, 1000);
    } catch (error) {
      next(error);
    }
  };
}

export default FlightsController;

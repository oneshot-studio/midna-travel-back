const { MongoClient, listDatabases } = require('mongodb');
class MongoConnection {
  private pass = '#L*6j@IkqX&R';
  private adminPassword = encodeURIComponent(this.pass);

  async listDatabases(client) {

    const databasesList = await client.db().admin().listDatabases();

    console.log('Databases:');
    databasesList.databases.forEach(db => console.log(` - ${db.name}`));
  }

  async main() {
    /**
     * Connection URI. Update <username>, <password>, and <your-cluster-url> to reflect your cluster.
     * See https://docs.mongodb.com/ecosystem/drivers/node/ for more details
     */
    const uri = `mongodb+srv://midnatravel:${this.adminPassword}@cluster0.swhsa.mongodb.net/test?retryWrites=true&w=majority`;

    const client = new MongoClient(uri);
    try {
      // Connect to the MongoDB cluster
      await client.connect();
      // Make the appropriate DB calls
      await listDatabases(client);
      await this.listDatabases(client);
    } catch (e) {
      console.error(e);
    } finally {
      await client.close();
    }
  }

  //   public async testMongo() {
  //     try {

  //       const uri = `mongodb+srv://midnatravel:${this.adminPassword}@cluster0.swhsa.mongodb.net/test?retryWrites=true&w=majority`;
  //       const client = new MongoClient(uri);
  //       await client.connect();
  //       await listDatabases(client);

  //     //   const cursor = client.db('midnatravel').collection('users').find();
  //     //   console.log(cursor);

  //     //   const results = await cursor.toArray();
  //     //   client.connect(data => {
  //     //     console.log(data);
  //     //     const collection = client.db('midnatravel').collection('users');
  //     //     // perform actions on the collection object
  //     //     // client.close();
  //     //   });
  //     //     const cursor = client.db('midnatravel').collection('users').find();
  //     //   console.log(cursor);
  //       //   console.log(cursor);
  //       //   const results = await cursor.toArray();
  //       //   if (results.length > 0) {
  //       //     console.log(`Found ${results.length} listing(s):`);
  //       //     results.forEach((result, i) => {
  //       //       console.log(result);
  //       //     });
  //       //   }
  //     } catch (error) {
  //       console.log(error);
  //     } finally {
  //         await client.close();
  //     }
  //   }
}
export default MongoConnection;

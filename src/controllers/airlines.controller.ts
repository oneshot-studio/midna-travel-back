import { NextFunction, Request, Response } from 'express';
import { CreateAirlineDto } from '@dtos/airline.dto';
import { Airline } from '@interfaces/airline.interface';
import AirlineService from '@services/airline.service';

class AirlineController {
  public airlineService = new AirlineService();

  public getAirlines = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const data: Airline[] = await this.airlineService.findAllAirlines();
      setTimeout(() => {
        res.status(200).json(data);
      }, 1000);
    } catch (error) {
      next(error);
    }
  };
}

export default AirlineController;

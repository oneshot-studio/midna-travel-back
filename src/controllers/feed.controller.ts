import { NextFunction, Request, Response } from 'express';
import { FeedDto } from '@dtos/feed.dto';
import { Feed } from '@interfaces/feed.interface';
import FeedService from '@services/feed.service';

class FeedController {
  public feedService = new FeedService();

  public getAllFeed = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const data: Feed[] = await this.feedService.findAllFeed();
      setTimeout(() => {
        res.status(200).json(data);
      }, 1000);
    } catch (error) {
      next(error);
    }
  };

  public testMongo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    this.feedService.testMongo();
  };
}

export default FeedController;

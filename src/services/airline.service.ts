import { HttpException } from '@exceptions/HttpException';
import { Airline } from '@interfaces/airline.interface';
import { airlinesModel } from '@models/airlines.model';

class AirlineService {
  public airlines = airlinesModel;

  public async findAllAirlines(): Promise<Airline[]> {
    const airlines: Airline[] = this.airlines.slice(0, 10);
    return airlines;
  }
}

export default AirlineService;

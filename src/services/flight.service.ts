import { HttpException } from '@exceptions/HttpException';
import { Flight } from '@interfaces/flight.interface';
import { flightModel, flightModelHistory } from '@models/flight.model';

class FlightService {
  public flight = flightModel;
  public flightHistory = flightModelHistory;

  public async findAllflights(): Promise<Flight> {
    const airline: Flight = this.flight;
    return airline;
  }

  public async findAllflightsHistory(): Promise<Flight> {
    const flightHistory: Flight = this.flightHistory;
    return flightHistory;
  }

  // public async findFlightById(userId: number): Promise<Flight> {
  //   const findFlight: Flight = this.users.find(user => user.id === userId);
  //   if (!findFlight) throw new HttpException(409, "You're not user");
  //   return findFlight;
  // }
}

export default FlightService;

import MongoConnection from '@/controllers/mongoConnection';
import { HttpException } from '@exceptions/HttpException';
import { Feed } from '@interfaces/feed.interface';
import { airlinesModel } from '@models/airlines.model';
import { cli } from 'winston/lib/winston/config';
//mongo dev
// import { MongoClient, ServerApiVersion } from 'mongodb';

class FeedService {
  public airlines = airlinesModel;
  public async findAllFeed(): Promise<Feed[]> {
    const airlines: Feed[] = this.airlines.slice(0, 10);
    return airlines;
  }

  public async testMongo() {
    const connection = new MongoConnection();
    connection.main().catch(console.error);
  }
}
export default FeedService;

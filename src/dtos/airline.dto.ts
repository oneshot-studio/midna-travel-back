import { IsEmail, IsString } from 'class-validator';
export class CreateAirlineDto {
  @IsEmail()
  public email: string;

  @IsString()
  public password: string;
}

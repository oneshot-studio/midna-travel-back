import { IsEmail, IsString } from 'class-validator';

export class CreateFlightDto {
  @IsEmail()
  public email: string;

  @IsString()
  public password: string;
}

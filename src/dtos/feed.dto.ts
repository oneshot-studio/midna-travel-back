import { IsEmail, IsString } from 'class-validator';

export class FeedDto {
  @IsEmail()
  email: string;

  @IsString()
  password: string;
}
